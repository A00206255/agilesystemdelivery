import java.sql.ResultSet;

public class Docket {
	private String customer_firstName;
	private String customer_lastName;
	private String DeliveryPerson_firstName;
	private String DeliveryPerson_lastName;
	private String deliveryArea;
	private int del_pers_id;
	private int docket_cust_id;
	Database myDB = null;
	
	private int search_id;

	public Docket(String cf, int dp, int c, Database d) {
		this.deliveryArea = cf;
		this.del_pers_id = dp;
		this.docket_cust_id = c;
		this.myDB = d;

	}

	public Docket(Database d) {
		this.myDB = d;

	}

	public boolean insertDocket() {
		boolean insert = myDB.addNewDocket(deliveryArea, del_pers_id, docket_cust_id);
		return insert;
	}
	

	public ResultSet searchDocket_DocketID(int dockId) 
	{
		this.search_id = dockId;
		
		ResultSet search = myDB.searchDocket_DocketID(search_id);
		return search;
	}

	public ResultSet searchDocket_CustID(int cusId) 
	{
		this.search_id = cusId;
		
		ResultSet search = myDB.searchDocket_CustID(search_id);
		return search;
	}
}
