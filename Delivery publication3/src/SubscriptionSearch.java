import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.ScrollPaneConstants;

public class SubscriptionSearch {

	JFrame frame;
	private JTextField textField;

	private Database dbase = null;
	private Subscription sc=null;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public SubscriptionSearch(String s,Database d,Subscription sub) {
		super();
		dbase=d;
		initialize();
		sc=sub;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 782, 586);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.BLACK);
		panel.setBorder(BorderFactory.createTitledBorder( "Search Subscriptions"));
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(39, 28, 574, 445);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(55, 95, 486, 122);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblSearchBySubscription = new JLabel("Search By Subscription ID:");
		lblSearchBySubscription.setBounds(37, 5, 167, 18);
		panel_1.add(lblSearchBySubscription);
		
		textField = new JTextField(10);
		textField.setBounds(37, 44, 251, 24);
		panel_1.add(textField);
		
		JButton button = new JButton("Search");
		button.setBounds(307, 43, 167, 27);
		panel_1.add(button);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(55, 230, 486, 109);
		panel.add(panel_2);
		
		JLabel lblSearchByCustomer = new JLabel("Search By Customer ID:");
		lblSearchByCustomer.setBounds(37, 5, 145, 18);
		panel_2.add(lblSearchByCustomer);
		
		textField_1 = new JTextField(10);
		textField_1.setBounds(37, 52, 251, 24);
		panel_2.add(textField_1);
		
		JButton button_1 = new JButton("Search");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Subscription s=new Subscription();
				try {
					
					ResultSet rs;
					
					rs=s.searchSubscription_CustID(Integer.parseInt(textField_1.getText()));

					if(rs != null)
					{
						String[] sub_details = new String[7];
						
						
						String indp = "Yes";
						String mir = "Yes";
						String topic = "Yes";
						String lead = "Yes";
						
						if(rs.getString("Independent") == null)
						{
							indp = "No";
						}
						else if(rs.getString("MIrror") == null)
						{
							mir = "No";
						}
						else if(rs.getString("Leader") == null)
						{
							topic = "No";
						}
						else if(rs.getString("Leader") == null)
						{
							lead = "No";
						}
						
					
						sub_details[0]="Holiday Start: "+rs.getString("Holiday_Start");
						sub_details[1]="Holiday End: "+rs.getString("Holiday_End");
						sub_details[2]="Subscribed to Independent: "+ indp;
						sub_details[3]="Subscribed to Mirror: "+mir;
						sub_details[4]="Subscribed to Leader: "+lead;
						sub_details[5]="Subscribed to Topic: "+topic;
						sub_details[6]="Customer ID: "+rs.getString("Cust_ID");
						
						String str="";
						for(int i=0;i<7;i++) 
						{
							System.out.println(sub_details[i]);
							str+=sub_details[i]+"\n";
						}
						
						JOptionPane.showMessageDialog(null, str);
					}
					else
					{
						textField.setText("");
					}
					
				} catch (NumberFormatException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_1.setBounds(313, 49, 161, 27);
		panel_2.add(button_1);
		
		JButton button_2 = new JButton("Cancel");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				//Code to open the main menu			
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
/*				setVisible(false); 
				dispose(); */
			}
		});
		button_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		button_2.setBounds(241, 365, 178, 40);
		panel.add(button_2);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		
				Subscription s=new Subscription();
			try {
				ResultSet rs;
				rs=s.searchSubscription_SubID(Integer.parseInt(textField.getText()));
				
				if(rs != null)
				{
					String[] sub_details = new String[7];
					
					String indp = "Yes";
					String mir = "Yes";
					String topic = "Yes";
					String lead = "Yes";
					
					if(rs.getString("Independent") == null)
					{
						indp = "No";
					}
					else if(rs.getString("MIrror") == null)
					{
						mir = "No";
					}
					else if(rs.getString("Leader") == null)
					{
						topic = "No";
					}
					else if(rs.getString("Leader") == null)
					{
						lead = "No";
					}
					
					sub_details[0]="Holiday Start: "+rs.getString("Holiday_Start");
					sub_details[1]="Holiday End: "+rs.getString("Holiday_End");
					sub_details[2]="Subscribed to Independent: "+ indp;
					sub_details[3]="Subscribed to Mirror: "+mir;
					sub_details[4]="Subscribed to Leader: "+lead;
					sub_details[5]="Subscribed to Topic: "+topic;
					sub_details[6]="Customer ID: "+rs.getString("Cust_ID");
					String str="";
					
					for(int i=0;i<7;i++) 
					{
						System.out.println(sub_details[i]);
						str+=sub_details[i]+"\n";
					
					}
					JOptionPane.showMessageDialog(null, str);
				}
				else
				{
					textField_1.setText("");
				}
			} 
			catch (NumberFormatException | SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		});
	}
}
